These three R scripts contain code for the analysis and figures in [Nguyen, M and Veraart, A.E.D. (2017), 'A note on confidence intervals for parameter estimates of a spatio-temporal Ornstein-Uhlenbeck process'. Report available at arXiv:1612.05462.](https://arxiv.org/abs/1612.05462). To reproduce the results in the paper, the scripts should be used in the following order:

1. 'CholeskySimulation.R': Generate 250 Cholesky datasets.
2. 'CLestimation.R': Conduct the composite likelihood estimation. 
3. 'MonteCarloCIs.R': Compute Monte Carlo confidence intervals and coverages.


Please do let me know if you encounter any errors.