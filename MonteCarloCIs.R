## ------  DG and Cholesky Monte Carlo confidence intervals ----- #

## Retrieve MM estimates of 250 initial Cholesky data sets (101x101) #

MMestimates <- read.delim("Choln101_MM.txt", header = FALSE)
# g; hatl; hatc; hattau; hattmu; hatmu; hatsigma2.
# True parameters:
tl <- 1; tc <- 1; ttau <- 0.1; ttmu <- 0.2; tmu <- 2*tc*ttmu/(tl^2); tsigma2 <- tc*(ttau^2)/(2*tl^2)

## Common set up for DG and Cholesky simulations ##
library(MASS)
dt <- 0.05
# Definition of the kernel in terms of x-xi = u, t-s = w:
h <- function(l, c, u, w){
  if ((abs(u) <= c*w) & (w >= 0)){
    exp(-l*w)
  }
  else{0}
}
intkn <- function(c, n, l){(2*c)/((n^2)*(l^2))}

# ---------- Coverage using DG simulations for CIs of MM estimates ---------- #

# Initial coverage vector to be updated later:
coverage <- rep(0, 6)

for (g in 1:100){
  dt <- 0.05
  hatl <- MMestimates[g, 2]; hatc <- MMestimates[g, 3]; 
  hattau <- MMestimates[g, 4]; hattmu <- MMestimates[g, 5];
  hatmu <- MMestimates[g, 6]; hatsigma2 <- MMestimates[g, 7]
  # Grid coordinates:
  xval <- (0:100)*hatc*dt; tval <- (0:100)*dt
  nx <- length(xval); nt <- length(tval)
  
  ## Generate data sets from fitted model and for each, compute the MM estimate ##
  
  # Parameter vector: 
  pv <- c(hattmu, hattau)
  
  # Discretisation of kernel:
  p <- 300; q <- 300
  # Even numbers.
  
  wvect <- dt*seq(0, p, by = 1)
  uvect <- hatc*dt*seq(-q, q, by = 1)
  hmat <- matrix(0, nrow = length(uvect), ncol = length(wvect))
  for (i in (1:length(uvect))){
    for (j in (1:length(wvect))){
      k <- i + j
      if (k %% 2 == 0){
        hmat[i, j] <- h(hatl, hatc, uvect[i], wvect[j])
      }
    }
  }
  
  # List of rows:
  htab <- split(hmat, row(hmat))
  # kernel is symmetrical in u.
  dimhx <- dim(hmat)[1]; dimht <- dim(hmat)[2]
  
  # Dimensions of extended simulation grid for W:
  nr <- nx + dimhx - 1; nc <- nt + dimht - 1
  
  for (g2 in 1:100){
    
    # Generate data.
    set.seed(g2)
    temptime <- proc.time()[3]
    Wmat <- matrix(0, nrow = nr, ncol = nc)
    for (i in 1:nr){
      for (j in 1:nc){
        k <- i + j
        if (k %% 2 == 0){
          Wmat[i, j] <- rnorm(1, mean = 2*hatc*(dt^2)*pv[1], sd = sqrt(2*hatc*(dt^2)*(pv[2]^2)))
        }
      }
    }
    
    # List of rows:
    Wtab <- split(Wmat, row(Wmat))
    
    # Perform discrete convolution by row:
    Y <- rep(NA, nt)
    for (i in 1:nx){
      yvect <- rep(0, nt)
      # i = 1 (x[0]):
      padstart <- i 
      for (k in 1:dimhx){
        j <- padstart + k - 1
        yvect <- yvect + convolve(Wtab[[j]], htab[[k]], type = 'f')
      }
      Y <- rbind(Y, rev(yvect))
      # Time was reversed.
    }
    # Remove dummy first row:
    Y <- Y[-1, ]
    # Insert missing values:
    for (i in 1:nx){
      for (j in 1:nt){
        k <- i + j
        if (k %% 2 != 0){
          Y[i, j] <- NA
        }	
      }
    }
    
    # Save the data set (not required):
    #fname <- paste("MC_DGdatag", g, "n","101", g2, ".txt", sep = "")
    #write.matrix(Y, file = fname)
    
    # MM inference: 
    
    nrY <- nrow(Y); ncY <- ncol(Y);
    # Estimate kernel parameters
    ndata <- ((nrY-1)/2)*ncY + ceiling(ncY/2)
    # Sample variance
    s1 <- sum(Y, na.rm = TRUE); s2 <- sum(Y^2, na.rm = TRUE); 
    s3 <- sum(Y^3, na.rm = TRUE); s4 <- sum(Y^4, na.rm = TRUE); 
    k2 <- (1/(ndata*(ndata-1)))*(ndata*s2 - s1^{2})
    # Empirical variograms gamma(2*c*dx, 0) and gamma(0,2*c*dt)
    d01 <- Y; d01[, 1:(ncY-2)] <- d01[, 3:ncY]; d01[, (ncY-2+1):ncY] <- NA
    g01 <- mean((Y - d01)^2, na.rm = TRUE)/k2
    d10 <- Y; d10[1:(nrY - 2),] <- d10[3:nrY,]; d10[(nrY-2+1):nrY, ] <- NA
    g10 <- mean((Y - d10)^2, na.rm = TRUE)/k2
    
    # Changes with simulation grid.
    hatl1 <- -log(1 - g01/2)/(2*dt)
    hatc1  <- -hatl*(2*hatc*dt)/log(1 - g10/2)
    
    # Estimate Levy basis parameters (Non-parametric method)
    k1 <- (1/ndata)*s1
    kw1 <- k1/intkn(hatc1, 1, hatl1); kw2 <- k2/intkn(hatc1, 2, hatl1)
    hatmu1 <- kw1; hattau1 <- sqrt(kw2)
    
    ctime <- proc.time()[3] - temptime
    
    # Save results:	
    result <- data.frame("Trial" = NA, "hatl" = NA, "hatc" = NA, "hattau" = NA, "hattmu" = NA, "hatmu" = NA, "hatsigma2" = NA, "ctime" = NA)
    result$Trial <- g2
    result$hatl <- hatl1; result$hatc <- hatc1; 
    result$hattau <- hattau1; result$hattmu <- hatmu1; 
    result$hatmu <- k1; result$hatsigma2 <- k2; 
    result$ctime <- ctime;
    write.table(result, file = paste("MC",g,"_DGMM101.txt", sep = ''), sep = "\t", col.names = FALSE, 
                row.names = FALSE, append = TRUE)
  }
  
  DGres <- read.delim(paste("MC",g,"_DGMM101.txt", sep = ''), header = FALSE)
  # Remove seed column.
  results <- DGres[, -1]
  
  # 95% confidence interval and median:
  MCCI <- matrix(NA, nrow = 6, ncol = 3)
  # hatl: 
  MCCI[1, ] <- quantile(results[[1]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatc:
  MCCI[2, ] <- quantile(results[[2]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hattau:
  MCCI[3, ] <- quantile(results[[3]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hattmu:
  MCCI[4, ] <- quantile(results[[4]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatmu:
  MCCI[5, ] <- quantile(results[[5]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatsigma2:
  MCCI[6, ] <- quantile(results[[6]], probs = c(0.025, 0.5, 0.975), type = 7)
  
  if (tl <= MCCI[1, 3] && tl >= MCCI[1, 1]){
    coverage[1] <- coverage[1] + 1
  }
  if (tc <= MCCI[2, 3] && tc >= MCCI[2, 1]){
    coverage[2] <- coverage[2] + 1
  }
  if (ttau <= MCCI[3, 3] && ttau >= MCCI[3, 1]){
    coverage[3] <- coverage[3] + 1
  }
  if (ttmu <= MCCI[4, 3] && ttmu >= MCCI[4, 1]){
    coverage[4] <- coverage[4] + 1
  }
  if (tmu <= MCCI[5, 3] && tmu >= MCCI[5, 1]){
    coverage[5] <- coverage[5] + 1
  }
  if (tsigma2 <= MCCI[6, 3] && tsigma2 >= MCCI[6, 1]){
    coverage[6] <- coverage[6] + 1
  }
  
}

coverage
coverage/100 

# After simulations (if required):

coverage <- rep(0, 6)
tl <- 1; tc <- 1; ttau <- 0.1; tsigma2 <- 0.005; tmu <- 0.4; ttmu <- 0.2

for (g in 1:100){
  DGres <- read.delim(paste("MC",g,"_DGMM101.txt", sep = ''), header = FALSE)
  # Remove seed column.
  results <- DGres[, -1]
  
  # 95% confidence interval and median:
  MCCI <- matrix(NA, nrow = 6, ncol = 3)
  # hatl: 
  MCCI[1, ] <- quantile(results[[1]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatc:
  MCCI[2, ] <- quantile(results[[2]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hattau:
  MCCI[3, ] <- quantile(results[[3]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hattmu:
  MCCI[4, ] <- quantile(results[[4]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatmu:
  MCCI[5, ] <- quantile(results[[5]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatsigma2:
  MCCI[6, ] <- quantile(results[[6]], probs = c(0.025, 0.5, 0.975), type = 7)
  
  if (tl <= MCCI[1, 3] && tl >= MCCI[1, 1]){
    coverage[1] <- coverage[1] + 1
  }
  if (tc <= MCCI[2, 3] && tc >= MCCI[2, 1]){
    coverage[2] <- coverage[2] + 1
  }
  if (ttau <= MCCI[3, 3] && ttau >= MCCI[3, 1]){
    coverage[3] <- coverage[3] + 1
  }
  if (ttmu <= MCCI[4, 3] && ttmu >= MCCI[4, 1]){
    coverage[4] <- coverage[4] + 1
  }
  if (tmu <= MCCI[5, 3] && tmu >= MCCI[5, 1]){
    coverage[5] <- coverage[5] + 1
  }
  if (tsigma2 <= MCCI[6, 3] && tsigma2 >= MCCI[6, 1]){
    coverage[6] <- coverage[6] + 1
  }
}

coverage
coverage/100 

# ---------- Coverage using Cholesky simulations for CIs of MM estimates ---------- #

# Initial coverage vector to be updated later:
ch_coverage <- rep(0, 6)

# Setting up covariance matrix for m*m grid:
m <- 101
xcoord <- rep(seq(0, (m-1)*0.05, by = 0.05), m)
tcoord <- rep(seq(0, (m-1)*0.05, by = 0.05), each = m)
n <- length(xcoord)
Cm <- diag(n)

for (g in 1:100){
  hatl <- MMestimates[g, 2]; hatc <- MMestimates[g, 3]; 
  hattau <- MMestimates[g, 4]; hattmu <- MMestimates[g, 5];
  hatmu <- MMestimates[g, 6]; hatsigma2 <- MMestimates[g, 7]
  for (i in 1:(n-1)){
    for (j in (i+1):n){
      tdist <- abs(tcoord[i] - tcoord[j])
      sdist <- abs(xcoord[i] - xcoord[j])
      Ci <- exp(-hatl*max(tdist, sdist/hatc)) 
      Cm[i, j] <- Ci
      Cm[j, i] <- Ci
    }
  } 
  
  
  ## Generate data sets from fitted model and for each, compute the MM estimate ##
  
  # Cholesky Decomposition:
  Utri <- chol(hatsigma2*Cm) 
  Ltri <- t(Utri)
  
  for (g2 in 1:100){
    
    # Generate data.
    set.seed(g2)
    Yv <- hatmu + Ltri%*%rnorm(n)
    Y <- matrix(Yv, nrow = m, ncol = m, byrow = FALSE)
    
    # Save the data set (not required):
    #fname <- paste("Cholesky_DGdatag", g, "n","101", g2, ".txt", sep = "")
    #write.matrix(Y, file = fname)
    
    # MM inference: 
    
    # Estimate kernel parameters
    ndata <- nrow(Y)*ncol(Y)
    # Sample variance
    s1 <- sum(Y); s2 <- sum(Y^2);
    k2 <- (1/(ndata*(ndata-1)))*(ndata*s2 - s1^{2})
    # Empirical variograms gamma(dx, 0) and gamma(0,dt)
    d01 <- Y; d01[,-ncol(Y)] <- d01[, -1]; d01[, ncol(Y)] <- NA
    g01 <- mean((Y - d01)^2, na.rm = TRUE)/k2
    d10 <- Y; d10[-nrow(Y), ] <- d10[-1, ]; d10[nrow(Y), ] <- NA
    g10 <- mean((Y - d10)^2, na.rm = TRUE)/k2
    # Estimated lambdas
    dx <- 0.05; dt <- 0.05
    # Changes with simulation grid.
    hatl1 <- -log(1 - g01/2)/dt # l overestimated for small data (m = 11, 21).
    hatc1  <- -hatl1*dx/log(1 - g10/2)
    # Estimate Levy basis parameters (Non-parametric method)
    k1 <- (1/ndata)*s1
    kw1 <- k1/intkn(hatc1, 1, hatl1); kw2 <- k2/intkn(hatc1, 2, hatl1)
    hatmu1 <- kw1; hattau1 <- sqrt(kw2)
    
    # Save results:	
    result <- data.frame("Trial" = NA, "hatl" = NA, "hatc" = NA, "hattau" = NA, "hattmu" = NA, "hatmu" = NA, "hatsigma2" = NA, "ctime" = NA)
    result$Trial <- g2
    result$hatl <- hatl1; result$hatc <- hatc1; 
    result$hattau <- hattau1; result$hattmu <- hatmu1; 
    result$hatmu <- k1; result$hatsigma2 <- k2; 
    result$ctime <- ctime;
    write.table(result, file = paste("Cholesky",g,"_MM101.txt", sep = ''), sep = "\t", col.names = FALSE, 
                row.names = FALSE, append = TRUE)
  }
  
  Chres <- read.delim(paste("Cholesky",g,"_MM101.txt", sep = ''), header = FALSE)
  # Remove seed column.
  results <- Chres[, -1]
  
  # 95% confidence interval and median:
  MCCI <- matrix(NA, nrow = 6, ncol = 3)
  # hatl: 
  MCCI[1, ] <- quantile(results[[1]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatc:
  MCCI[2, ] <- quantile(results[[2]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hattau:
  MCCI[3, ] <- quantile(results[[3]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hattmu:
  MCCI[4, ] <- quantile(results[[4]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatmu:
  MCCI[5, ] <- quantile(results[[5]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatsigma2:
  MCCI[6, ] <- quantile(results[[6]], probs = c(0.025, 0.5, 0.975), type = 7)
  
  if (tl <= MCCI[1, 3] && tl >= MCCI[1, 1]){
    ch_coverage[1] <- ch_coverage[1] + 1
  }
  if (tc <= MCCI[2, 3] && tc >= MCCI[2, 1]){
    ch_coverage[2] <- ch_coverage[2] + 1
  }
  if (ttau <= MCCI[3, 3] && ttau >= MCCI[3, 1]){
    ch_coverage[3] <- ch_coverage[3] + 1
  }
  if (ttmu <= MCCI[4, 3] && ttmu >= MCCI[4, 1]){
    ch_coverage[4] <- ch_coverage[4] + 1
  }
  if (tmu <= MCCI[5, 3] && tmu >= MCCI[5, 1]){
    ch_coverage[5] <- ch_coverage[5] + 1
  }
  if (tsigma2 <= MCCI[6, 3] && tsigma2 >= MCCI[6, 1]){
    ch_coverage[6] <- ch_coverage[6] + 1
  }
  
}

ch_coverage
ch_coverage/100

# After simulations (if required):

ch_coverage1 <- rep(0, 6)
tl <- 1; tc <- 1; ttau <- 0.1; tsigma2 <- 0.005; tmu <- 0.4; ttmu <- 0.2

for (g in 1:100){
  Chres <- read.delim(paste("Cholesky",g,"_MM101.txt", sep = ''), header = FALSE)
  # Remove seed column.
  results <- Chres[, -1]
  # 95% confidence interval and median:
  MCCI <- matrix(NA, nrow = 6, ncol = 3)
  # hatl: 
  MCCI[1, ] <- quantile(results[[1]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatc:
  MCCI[2, ] <- quantile(results[[2]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hattau:
  MCCI[3, ] <- quantile(results[[3]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hattmu:
  MCCI[4, ] <- quantile(results[[4]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatmu:
  MCCI[5, ] <- quantile(results[[5]], probs = c(0.025, 0.5, 0.975), type = 7)
  # hatsigma2:
  MCCI[6, ] <- quantile(results[[6]], probs = c(0.025, 0.5, 0.975), type = 7)
  
  if (tl <= MCCI[1, 3] && tl >= MCCI[1, 1]){
    ch_coverage1[1] <- ch_coverage1[1] + 1
  }
  if (tc <= MCCI[2, 3] && tc >= MCCI[2, 1]){
    ch_coverage1[2] <- ch_coverage1[2] + 1
  }
  if (ttau <= MCCI[3, 3] && ttau >= MCCI[3, 1]){
    ch_coverage1[3] <- ch_coverage1[3] + 1
  }
  if (ttmu <= MCCI[4, 3] && ttmu >= MCCI[4, 1]){
    ch_coverage1[4] <- ch_coverage1[4] + 1
  }
  if (tmu <= MCCI[5, 3] && tmu >= MCCI[5, 1]){
    ch_coverage1[5] <- ch_coverage1[5] + 1
  }
  if (tsigma2 <= MCCI[6, 3] && tsigma2 >= MCCI[6, 1]){
    ch_coverage1[6] <- ch_coverage1[6] + 1
  }
}

ch_coverage1
ch_coverage1/100 
# 0.63, 0.94, 0.62, 0.62, 0.86, 0.65.

# Repeat for lambda = 2 and 4.

## ------------------- Coverage proxy --------------------- ##


# Compare coverage proxies for lambda = 1, 2 and 4:

# l #
# lambda = 1:
x <- MMresults[, 2]
lq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
lcproxy <- ecdf(x)(tl + (lq[2]-lq[1])) - ecdf(x)(tl - (lq[3] - lq[2]))
# lambda = 2:
x <- MMresults2[, 2]
lq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
lcproxy2 <- ecdf(x)(tl2 + (lq[2]-lq[1])) - ecdf(x)(tl2 - (lq[3] - lq[2]))
# lambda = 4:
x <- MMresults4[, 2]
lq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
lcproxy4 <- ecdf(x)(tl4 + (lq[2]-lq[1])) - ecdf(x)(tl4 - (lq[3] - lq[2]))

# c #
# lambda = 1:
x <- MMresults[, 3]
cq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
ccproxy <- ecdf(x)(tc + (lq[2]-lq[1])) - ecdf(x)(tc - (lq[3] - lq[2]))
# lambda = 2:
x <- MMresults2[, 3]
cq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
ccproxy2 <- ecdf(x)(tc + (lq[2]-lq[1])) - ecdf(x)(tc - (lq[3] - lq[2]))
# lambda = 4:
x <- MMresults4[, 3]
cq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
ccproxy4 <- ecdf(x)(tc + (lq[2]-lq[1])) - ecdf(x)(tc - (lq[3] - lq[2]))

# tmu #
# lambda = 1:
x <- MMresults[, 5]
tmuq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
tmucproxy <- ecdf(x)(ttmu + (lq[2]-lq[1])) - ecdf(x)(ttmu - (lq[3] - lq[2]))
# lambda = 2:
x <- MMresults2[, 5]
tmuq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
tmucproxy2 <- ecdf(x)(ttmu + (lq[2]-lq[1])) - ecdf(x)(ttmu - (lq[3] - lq[2]))
# lambda = 4:
x <- MMresults4[, 5]
tmuq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
tmucproxy4 <- ecdf(x)(ttmu + (lq[2]-lq[1])) - ecdf(x)(ttmu - (lq[3] - lq[2]))

# tau #
# lambda = 1:
x <- MMresults[, 4]
tauq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
taucproxy <- ecdf(x)(ttau + (lq[2]-lq[1])) - ecdf(x)(ttau - (lq[3] - lq[2]))
# lambda = 2:
x <- MMresults2[, 4]
tauq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
taucproxy2 <- ecdf(x)(ttau + (lq[2]-lq[1])) - ecdf(x)(ttau - (lq[3] - lq[2]))
# lambda = 4:
x <- MMresults4[, 4]
tauq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
taucproxy4 <- ecdf(x)(ttau + (lq[2]-lq[1])) - ecdf(x)(ttau - (lq[3] - lq[2]))

# mu #
# lambda = 1:
x <- MMresults[, 6]
muq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
mucproxy <- ecdf(x)(tmu + (lq[2]-lq[1])) - ecdf(x)(tmu - (lq[3] - lq[2]))
# lambda = 2:
x <- MMresults2[, 6]
muq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
mucproxy2 <- ecdf(x)(tmu2 + (lq[2]-lq[1])) - ecdf(x)(tmu2 - (lq[3] - lq[2]))
# lambda = 4:
x <- MMresults4[, 6]
muq <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
mucproxy4 <- ecdf(x)(tmu4 + (lq[2]-lq[1])) - ecdf(x)(tmu4 - (lq[3] - lq[2]))

# s2 #
# lambda = 1:
x <- MMresults[, 7]
s2q <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
s2cproxy <- ecdf(x)(ts2 + (lq[2]-lq[1])) - ecdf(x)(ts2 - (lq[3] - lq[2]))
# lambda = 2:
x <- MMresults2[, 7]
s2q <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
s2cproxy2 <- ecdf(x)(ts22 + (lq[2]-lq[1])) - ecdf(x)(ts22 - (lq[3] - lq[2]))
# lambda = 4:
x <- MMresults4[, 7]
s2q <- quantile(x, probs = c(0.025, 0.5, 0.975), type = 7)
s2cproxy4 <- ecdf(x)(ts24 + (lq[2]-lq[1])) - ecdf(x)(ts24 - (lq[3] - lq[2]))

